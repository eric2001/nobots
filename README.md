# NoBots
![NoBots](./screenshot.jpg) 

## Description
NoBots is a plugin for [Gallery 3](http://gallery.menalto.com/) which will add a few HTML meta tags to the top of your album and photo pages that will tell search engines such as Google and Yahoo to go away.  This plugin should only be installed if you don't want people to find your gallery through search engines.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

## History
**Version 1.1.2:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 08 August 2011. 
>
> Download: [Version 1.1.2](/uploads/1245da54111b18f01cbcc8d74861d7f7/nobots112.zip)

**Version 1.1.1:**
> - Fixed a few minor issues with the HTML code to make the module XHTML 1.0 Transitional complaint.
> - Released 14 June 2010.
>
> Download: [Version 1.1.1](/uploads/f9d3b02c1864b5a6ddd2a3e6e6858214/nobots111.zip)

**Version 1.1.0:**
> - Updated for recent module API changes in Gallery 3.
> - Released 20 January 2010.
>
> Download: [Version 1.1.0](/uploads/b14c2e09961b38d5fd1cf2cc35da24bc/nobots110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 17 June 2009.
>
> Download: [Version 1.0.0](/uploads/a310d7c1bd1f4a83632876cbfba33e98/nobots100.zip)
